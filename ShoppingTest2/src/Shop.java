import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Shop {

	public static void main(String[] args) {
		
		loginIntro();
		

	}
	public static void SignUp(HashMap users) {
		
		Scanner scan = new Scanner(System.in);
	
		System.out.println("Please enter a username: ");
		String tempUser = scan.nextLine();
		if(users.containsKey(tempUser))
		{
			System.out.println("That username is already taken. Please enter a new one");
			SignUp(users);
		}
		else
		{	
			CreatePassword(users, tempUser);
		}
	}

	public static void CreatePassword(HashMap users, String tempUser)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a password that has at least one uppercase letter, one lower case letter, one digit, and be at least 6 characters long: ");
		String tempPass = scan.nextLine();
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,}";
		if(tempPass.matches(pattern))
		{
			users.put(tempUser, tempPass);
			System.out.println("You have now made a new account");
		}
		else
		{
			System.out.println("Please enter a new password that meets the requirements");
			CreatePassword(users, tempUser);
		}
	}
	public static void Login(HashMap users) {
		
		Scanner scan = new Scanner(System.in);	
	
		System.out.println("Please enter your username: ");
		String checkUser = scan.nextLine();
		if(users.containsKey(checkUser))
		{
			PasswordLogin(users, checkUser);
		}
		else
		{
			System.out.println("Incorrect username. Please try again");
			Login(users);
		}
	}
	
	public static void PasswordLogin(HashMap users, String checkUser)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter your password: ");
		String checkPass = scan.nextLine();
		if(users.get(checkUser).equals(checkPass))
		{
			System.out.println("Login success!\n");
			shopFile();
			shoppingCart();
		}
		else
		{
			System.out.println("Incorrect password. Please try again.");
			PasswordLogin(users, checkUser);
		}
	}
	

	public static void loginIntro() {
		Scanner scan = new Scanner(System.in);
		HashMap<String, String> users = new HashMap<String, String>();
		boolean doAgain = true;
		
			while(doAgain) {
			System.out.println("If you would like to sign up, please enter 1. If you would like to login, please enter 2");
			int signOrLog = scan.nextInt();
			if(signOrLog ==1)
			{
				SignUp(users);
			}
			else
			{
				Login(users);
			}
			}
	}
	public static void shopFile() {
		Scanner sc = new Scanner(System.in);
		HashMap<String, Double> hm = new HashMap<String, Double>();
		ArrayList<String> items = new ArrayList<String>();
		ArrayList<Double> prices = new ArrayList<Double>();
		
		 File file = new File("C:\\Users\\nmuel\\OneDrive\\Documents\\CS203\\Final\\list.txt");

		    try {
		        Scanner scan = new Scanner(file);
		        
		        	while(scan.hasNextLine()) {
		        			String item = scan.nextLine();
		        			if(item.equals("")) {
		        				item = scan.nextLine();
		        				items.add(item);
		            	
		        			}           	 
		            	 String input[] = scan.nextLine().split(":");
		            	 double price = Double.parseDouble(input[input.length - 1]);
		            	 prices.add(price);
		            	 	            	            			        		                		            	
		            }
		        		
		        	//System.out.println(prices);
		        	//System.out.println(items);
		        	for(int i = 0; i < items.size(); i++) {
		        		hm.put(items.get(i), prices.get(i));
		        		}
		        		           
				scan.close();	
		 
		        //System.out.println(hm);
		        System.out.println("Here are the items we have in stock:\n");
		        for(int i = 0; i < hm.size(); i++) {
		        	System.out.println(items.get(i) + " - " + prices.get(i));
		        }
		            
		       
		    }
		    catch (FileNotFoundException e) {
		        e.printStackTrace();
		       }
	}
	public static void shoppingCart() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the items you would like to add to your cart");
		
		System.out.println("Would you like to add another item");
		
	}
	
}
