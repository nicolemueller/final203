import java.util.*;

public class Person {
	String password;
	ArrayList<LinkedHashMap<String,Double>> allOrders = new ArrayList<LinkedHashMap<String, Double>>();
	LinkedHashMap<String, Double> order = new LinkedHashMap<String, Double>();
	Person(String password){
		this.password = password;
	}
	public String toString() {
		return password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void doneOrdering() {
		allOrders.add(order);
		order = new LinkedHashMap<String, Double>();
	}
	public void setOrder(String item, Double price) {
		order.put(item, price);
		
	}
	public LinkedHashMap<String, Double> getOrder(){
		return allOrders.get(allOrders.size()-1);
	}	
	public ArrayList<LinkedHashMap<String,Double>> getAllOrders(){
		return allOrders;
	}
}
