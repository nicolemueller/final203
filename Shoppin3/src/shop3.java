import java.io.*;
import java.util.*;

public class shop3 {
	static HashMap<String, Double> hm = new HashMap<String, Double>();
	static HashMap<String, Person> users = new HashMap<String, Person>();
	
	static ArrayList<String> items = new ArrayList<String>();
	static ArrayList<Double> prices = new ArrayList<Double>();
	
	public static void main(String[] args) {
		
		loginIntro();
		

	}
	public static void SignUp(HashMap users) {
		
		Scanner scan = new Scanner(System.in);
	
		System.out.println("Please enter a username: ");
		String tempUser = scan.nextLine();
		if(users.containsKey(tempUser))
		{
			System.out.println("That username is already taken. Please enter a new one");
			SignUp(users);
		}
		else
		{	
			CreatePassword(users, tempUser);
		}
	}
	public static void CreatePassword(HashMap users, String tempUser)
	{	
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a password that has at least one uppercase letter, one lower case letter, one digit, and be at least 6 characters long: ");
		String tempPass = scan.nextLine();
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,}";
		if(tempPass.matches(pattern))
		{
			users.put(tempUser, new Person(tempPass));
			System.out.println("You have now made a new account");
			//System.out.println(users.get(tempUser));
			loginIntro();
		}
		else
		{
			System.out.println("Please enter a new password that meets the requirements");
			CreatePassword(users, tempUser);
		}
	}
	public static void Login(HashMap users) {
		
		Scanner scan = new Scanner(System.in);	
	
		System.out.println("Please enter your username: ");
		String checkUser = scan.nextLine();
		if(users.containsKey(checkUser))
		{
			PasswordLogin(users, checkUser);
		}
		else
		{
			System.out.println("Incorrect username. Please try again");
			Login(users);
		}
	}
	public static void PasswordLogin(HashMap users, String checkUser)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter your password: ");
		String checkPass = scan.nextLine();
		Person pass = getPass(checkPass, users);
		if(pass == null) {
			System.out.println("Incorrect password. Please try again.");
			PasswordLogin(users, checkUser);
		}
		else if(pass.getPassword().equals(checkPass))
		{
			System.out.println("Login success!\n");
			shopFile();
			shoppingCart(checkUser);
		}
		else
		{
			System.out.println("Incorrect password. Please try again.");
			PasswordLogin(users, checkUser);
		}
	}
	public static Person getPass(String password, HashMap<String, Person> users) {
	    for (Person p : users.values()) {
	        if (p.getPassword().equals(password))
	           return p;
	    }
	    return null;
	}
	public static void loginIntro() {
		Scanner scan = new Scanner(System.in);
					
			System.out.println("If you would like to sign up, please enter 1. If you would like to login, please enter 2");
			int signOrLog = scan.nextInt();
			if(signOrLog ==1)
			{
				SignUp(users);
			}
			else
			{
				Login(users);
			}
			
	}
	public static void shopFile() {
		Scanner sc = new Scanner(System.in);
		
		 File file = new File("C:\\Users\\nmuel\\OneDrive\\Documents\\CS203\\Final\\list.txt");

		    try {
		        Scanner scan = new Scanner(file);
		        
		        	while(scan.hasNextLine()) {
		        			String item = scan.nextLine();
		        			if(item.equals("")) {
		        				item = scan.nextLine();
		        				items.add(item);
		        			}           	 
		            	 String input[] = scan.nextLine().split(":");
		            	 double price = Double.parseDouble(input[input.length - 1]);
		            	 prices.add(price);		            	 	            	            			        		                		            	
		            }
		        	for(int i = 0; i < items.size(); i++) {
		        		hm.put(items.get(i), prices.get(i));
		        		}	        		           
				scan.close();	
		        System.out.println("Here are the items we have in stock:\n");
		        for(int i = 0; i < hm.size(); i++) {
		        	System.out.println(items.get(i) + " - " + prices.get(i));
		        }		       
		    }
		    catch (FileNotFoundException e) {
		        e.printStackTrace();
		       }
	}
	public static void shoppingCart(String user) {
		Scanner scan = new Scanner(System.in);
		//logout(user);
		boolean addItem = true;
		System.out.println("Would you like to see your previous orders? yes or no");
		String prevOrder = scan.nextLine();
		if(prevOrder.equalsIgnoreCase("yes")) {
			System.out.println(users.get(user).getAllOrders());
		}
		
		while(addItem) {
			System.out.println("Please enter the items you would like to add to your cart");
			String item = scan.nextLine();
			int index = items.lastIndexOf(item);
			if (index != -1) {
				users.get(user).setOrder(item, prices.get(index));
			}
			System.out.println("Would you like to add another item. Please enter yes or no");
			String yOrNo = scan.nextLine();
			if(!yOrNo.equalsIgnoreCase("yes")) {
				addItem = false;
			}
		}
		users.get(user).doneOrdering();
		System.out.println("Your order has been placed");
		System.out.println(users.get(user).getOrder());
		logout(user);
	}
	public static void logout(String user) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Would you like to logout? yes or no");
		String logout = scan.nextLine();
		if(logout.equalsIgnoreCase("yes")) {
			System.out.println("You are now logged out");
			loginIntro();
		}
		else {
			shoppingCart(user);
		}
		
	}
	
}
